// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];
var savedRegionsList = [];
let geod = GeographicLib.Geodesic.WGS84;
let distancePosts = 4; //Default fence posts value in metres
let keyItem = localStorage.getItem("DISTANCE") //Declare variables for usage of local storage

//Parse to become Javascript Object
if (keyItem !== null){
    distancePosts = JSON.parse(localStorage.getItem("DISTANCE")); //To be used in settings.js
}

// Feature 1
// Class named Region
class Region {
    constructor () {
	// Private attributes
    this._cornerLocationsArray = [];
   	this._dateAndTime = new Date().getDate() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getFullYear() + "  " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds(); //For month +1 because it's from 0-11
    }
    
    // Public methods
    get cornerLocationsArray(){
        return this._cornerLocationsArray;
    }

    addCornerToArray(newRegion){
        this.cornerLocationsArray.push(newRegion); //Adds new region to cornerLocationsArray
    }
    
    get dateAndTime(){
        return this._dateAndTime;
    }
    
    set dateAndTime(newDate){
        this._dateAndTime = newDate;
    }

    toString(){
        return "The Region is:"+ this._cornerLocationsArray
    }
    
    initialiseCornerPDO(regionObj){
        this._cornerLocationsArray =regionObj._cornerLocationsArray
    }

    area(){
        return this.getArea();
    }
    
    perimeter(){
        return this.getPerimeter();
    }
    
    getArea(){
        let regionArea = geod.Polygon(); //Calculate
        for (let i = 0; i < this._cornerLocationsArray.length; i++){
            regionArea.AddPoint(this._cornerLocationsArray[0][i][1]); //Area
        }
        regionArea = regionArea.Compute(true,true);
        //Return area
        return regionArea.area.toFixed(2);
    }
    
    getPerimeter(){
        let regionPerimeter = geod.Polygon(); //Calculate perimeter
        for (let i = 0; i < this._cornerLocationsArray.length; i++){
            regionPerimeter.AddPoint(this._cornerLocationsArray[0][i][0]); //Perimeter
        }
        regionPerimeter = regionPerimeter.Compute(true,true);
        //Return perimeter
        return regionPerimeter.perimeter.toFixed(2);
    }
    
    getfencePost() {
        let fencePostArray = [];
        for (let i = 0; i < this._cornerLocationsArray.length - 1; i++) {
            let result = geod.Inverse(this._cornerLocationsArray[i][1], this._cornerLocationsArray[i][0], this._cornerLocationsArray[i+1][1], this._cornerLocationsArray[i+1][0]);

            let next = geod.Direct(this._cornerLocationsArray[i][1], this._cornerLocationsArray[i][0], result.azi1, distancePosts);
            fencePostArray.push([next.lon2, next.lat2]);
        }
        return fencePostArray;
    }

}

// Feature 2
// Class name RegionList
class RegionList {
    constructor () {
	// Private attributes
	this._regions = []; // Same as Week 5 Prac in Deck Class
    this._numberOfRegions = this._regions.length // Number of elements in this._regions
    }
    
    //Public Methods
    get regions(){
        return this._regions;
    }
        
    addRegions(name){
        this.regions.push(name); //Adds new name to regions array
    }
    
    removeRegions(index) {
        this._regions.splice(index, 1);
    }
    
    get numberOfRegions() {
        return this._regions.length;
    }
    
    set numberOfRegions(number) {
        this._numberOfRegions = number;
    }

    initialObjPDO(initialItem) {
        var newRegion = new Region();
        newRegion.initialiseCornerPDO(initialItem);
        this._list = new Region(newRegion_cornerLocationsArray)
        console.log(this._list)
    }

}