document.getElementById('setDistance').innerHTML = 'Maximum distance between fence posts: '.bold() + distancePosts + ' m'; //To be displayed on the main settings page

//Reset to default value
function defaultPostsDistance(){
    localStorage.removeItem("DISTANCE"); //Removes number from DISTANCE Storage
    document.getElementById('setDistance').innerHTML = 'Maximum distance between fence posts: '.bold() + distancePosts + ' m';
}

//Set Post Distance Value
function setPostsDistance(){
    let value = prompt('Set value for maximum post distance!');
    if (value === '' || isNaN(value)) {
        alert('Please enter a number! Try again.')
        defaultPostsDistance(); //Resets to original value
    } 
    else {
        localStorage.setItem("DISTANCE", value); //Sets value and place into DISTANCE Storage
        document.getElementById('setDistance').innerHTML = 'Maximum distance between fence posts: '.bold() + value + ' m';
    }
}