// Code for the main app page (Regions List).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

function viewRegion(regionIndex)
{
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", regionIndex); 
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}

//To change index.html
let regionListElement = document.getElementById("regionsList")
let listHTML = "";
let retrieveStorage = JSON.parse(localStorage.getItem("REGIONS")) //From Region class
let retrieveStorage1 = JSON.parse(localStorage.getItem("REGIONSLIST")) //From RegionList class

for (let i = 0 ; i < retrieveStorage.length ; i++){
        //Replicate the sample from index.html but in javascript form
        listHTML += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRegion("+i+")\">" ;
        listHTML += "<span class=\"mdl-list__item-primary-content\">";
        listHTML += "<span>" + retrieveStorage1[i]._regions + "</span>" //Get nameThisRegion from RegionList class
        listHTML += "<span class=\"mdl-list__item-sub-title\">" + retrieveStorage[i]._dateAndTime + "</span>" //Get the date and time from Region Class
        listHTML += "</span>"
        listHTML += "</li>"
        
        //Display to HTML
        regionListElement.innerHTML = listHTML;
}
