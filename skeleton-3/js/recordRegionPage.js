// Code for the Record Region page.

mapboxgl.accessToken = 'pk.eyJ1IjoiamFzb24td29zaGluaWJhYmEiLCJhIjoiY2tjd3cwNHBhMDZnNzJxbG03N245ZDRsMyJ9.jR6scH4tx8L4W-Rdh3ACYw';

//Generate new map
var map = new mapboxgl.Map({
container: 'map', // container id
style: 'mapbox://styles/mapbox/streets-v11',
center: [144.9619, -37.8164], // starting position
zoom: 11 // starting zoom
});

let marker1; //For placing markers
var marker=[]; //Placed markers stored in array

map.on('mousemove', function (e) {
document.getElementById("info").innerHTML =
// e.point is the x, y coordinates of the mousemove event relative
// to the top-left corner of the map
JSON.stringify(e.point) +
'<br />' +
// e.lngLat is the longitude, latitude geographical position of the event
JSON.stringify(e.lngLat.wrap());
});

map.on('click', function (e) {
    if (marker1 !== undefined){
        marker1.remove(map)
    }
                           
    marker1 = new mapboxgl.Marker() //Place new marker
        .setLngLat(e.lngLat)
        .addTo(map)
    map.panTo(e.lngLat) //Pan and hover around the map
});

function addCorner(e){
    let LngLat = marker1.getLngLat(); //Get the coordinates in LAT and LNG
    marker.push(LngLat) //Coordinates stored in marker array
    console.log(marker)
    
    if (marker.length>1 && marker.length<3){ //There is a bug where shape doesn't show with marker.length === 1, so assume marker.length is more than 1 but less than 3
map.addSource('region', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'LineString',
'coordinates': [
[marker[0].lng , marker[0].lat],
[marker[1].lng , marker[1].lat]

]
}
}
});
map.addLayer({
'id': 'region',
'type': 'fill',
'source': 'region',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});

}
   
   else if (marker.length>2 && marker.length<4){
map.removeLayer('region'); //Remove previous shape
map.removeSource('region');
map.addSource('region1', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'LineString',
'coordinates': [
[marker[0].lng , marker[0].lat],
[marker[1].lng , marker[1].lat],
[marker[2].lng , marker[2].lat]
    
]
}
}
});
map.addLayer({
'id': 'region1',
'type': 'fill',
'source': 'region1',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});

document.getElementById("saveRegion").disabled = false; //When 3 markers placed, button is clickable
}

    else if(marker.length >3 && marker.length<5 ){ 
map.removeLayer('region1');
map.removeSource('region1');
map.addSource('region2', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'LineString',
'coordinates': [
[marker[0].lng , marker[0].lat],
[marker[1].lng , marker[1].lat],
[marker[2].lng , marker[2].lat], 
[marker[3].lng , marker[3].lat]
 
]
}
}
});

map.addLayer({
'id': 'region2',
'type': 'fill',
'source': 'region2',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
}
    
    else if(marker.length >4 && marker.length<6 ){ 
map.removeLayer('region2');
map.removeSource('region2');
map.addSource('region3', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'LineString',
'coordinates': [
[marker[0].lng , marker[0].lat],
[marker[1].lng , marker[1].lat],
[marker[2].lng , marker[2].lat], 
[marker[3].lng , marker[3].lat],
[marker[4].lng , marker[4].lat]
 
]
}
}
});

map.addLayer({
'id': 'region3',
'type': 'fill',
'source': 'region3',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
}
    
    else if(marker.length >5 && marker.length<7 ){ 
map.removeLayer('region3');
map.removeSource('region3');
map.addSource('region4', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'LineString',
'coordinates': [
[marker[0].lng , marker[0].lat],
[marker[1].lng , marker[1].lat],
[marker[2].lng , marker[2].lat], 
[marker[3].lng , marker[3].lat],
[marker[4].lng , marker[4].lat],
[marker[5].lng , marker[5].lat]
 
]
}
}
});

map.addLayer({
'id': 'region4',
'type': 'fill',
'source': 'region4',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
}
    else if(marker.length >6 && marker.length<8 ){ 
map.removeLayer('region4');
map.removeSource('region4');
map.addSource('region5', {
'type': 'geojson',
'data': {
'type': 'Feature',
'geometry': {
'type': 'LineString',
'coordinates': [
[marker[0].lng , marker[0].lat],
[marker[1].lng , marker[1].lat],
[marker[2].lng , marker[2].lat], 
[marker[3].lng , marker[3].lat],
[marker[4].lng , marker[4].lat],
[marker[5].lng , marker[5].lat],
[marker[6].lng , marker[6].lat] 
]
}
}
});

map.addLayer({
'id': 'region5',
'type': 'fill',
'source': 'region5',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
}

    
}


function reset(){
    marker1.remove(map)
    if (marker.length===2){
        map.removeLayer('region');
        map.removeSource('region');
    }
    else if (marker.length===3){
        map.removeLayer('region1');
        map.removeSource('region1');
    }
    else if (marker.length===4){
        map.removeLayer('region2'); //Remove Layer first before removing source ID
        map.removeSource('region2');
    }
    else if (marker.length===5){
        map.removeLayer('region3'); //Remove Layer first before removing source ID
        map.removeSource('region3');
    }
     else if (marker.length===6){
        map.removeLayer('region4'); //Remove Layer first before removing source ID
        map.removeSource('region4');
    }
    else if (marker.length===7){
        map.removeLayer('region5'); //Remove Layer first before removing source ID
        map.removeSource('region5');
    }
    marker = []; //Marker array becomes empty
    document.getElementById("saveRegion").disabled = true; //Save Region button becomes unclickable
    }


function saveRegion(){
    //Region name to push in as constructor
    let nameThisRegion = [];

    //Asks
    if (confirm("Do you want to save this region?")) //In case of misclick  
    {
        //Prompt for a name for the region
        nameThisRegion = prompt("Name this Region!") 
        console.log("Region Saved")
        alert("Region Saved!")}
    
    if(typeof(Storage) !== "undefined"){
            //Declare variables for usage of local storage
            let keyItem = localStorage.getItem("REGIONS")
            let keyItem1 = localStorage.getItem("REGIONSLIST")
            let regionInstance = new Region();
            let regionListInstance = new RegionList();
            
            regionInstance.addCornerToArray(marker); //Adding coordinates into Region
            regionListInstance.addRegions(nameThisRegion) //Adding name into RegionList
        
            let JSONForm= JSON.stringify(regionInstance);
            let JSONForm1 = JSON.stringify(regionListInstance);
            let setItem = localStorage.setItem(keyItem,JSONForm,keyItem1,JSONForm1);
            
            //Check if local storage with key "REGIONS" and "REGIONSLIST" already has something
            if (keyItem !== null){
                //Parse existing class to push again with new class into local storage array
                let checkStorage = JSON.parse(localStorage.getItem("REGIONS"));
                let checkStorage1 = JSON.parse(localStorage.getItem("REGIONSLIST"));
                checkStorage.push(regionInstance);
                checkStorage1.push(regionListInstance);
                localStorage.setItem("REGIONS", JSON.stringify(checkStorage));
                localStorage.setItem("REGIONSLIST", JSON.stringify(checkStorage1));
            }
            
            else{
                //Push class into array to local storage
                savedRegions.push(regionInstance);
                savedRegionsList.push(regionListInstance);
                localStorage.setItem("REGIONS", JSON.stringify(savedRegions));
                localStorage.setItem("REGIONSLIST", JSON.stringify(savedRegionsList));
            }  
    }
    
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
}