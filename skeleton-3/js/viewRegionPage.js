// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
 mapboxgl.accessToken = 'pk.eyJ1IjoiamFzb24td29zaGluaWJhYmEiLCJhIjoiY2tjd3cwNHBhMDZnNzJxbG03N245ZDRsMyJ9.jR6scH4tx8L4W-Rdh3ACYw';

let arrLocations = []; //Corner Locations to be stored here
let arrRegionNames = []; //Region names to be stored here
let arrfencePosts = []; //Fence Posts to be stored here
var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion");
let retrieveStorage = JSON.parse(localStorage.getItem("REGIONS")); //From Region class
console.log(retrieveStorage);
let retrieveStorage1 = JSON.parse(localStorage.getItem("REGIONSLIST")); //From RegionList class
let regionInstance = new Region(); //Make new instance

if (regionIndex !== null)
{
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
    
        for (let i = 0 ; i < retrieveStorage.length ; i++)
    {
        //Push private attributes of every region class into array
        arrLocations.push(retrieveStorage[i]._cornerLocationsArray); //Corner Locations
        arrRegionNames.push(retrieveStorage1[i]._regions) //Region names
        
    }
    
    var regionNames = [ "Region A", "Region B" ];
    //Region names to be displayed in the header bar
    document.getElementById("headerBarTitle").textContent = arrRegionNames[regionIndex]; 
}

//Testings
let test = Object.values(arrLocations[regionIndex][0][0]);
let test1 = Object.values(arrLocations[regionIndex][0][1]);
let test2 = Object.values(arrLocations[regionIndex][0][2]);
console.log(test);
console.log(test1);
console.log(test2);
console.log(test[0]);
console.log(test[1]);
console.log(arrLocations[regionIndex][0].length)

//Generate new map
var map = new mapboxgl.Map({
container: 'map', // container id
style: 'mapbox://styles/mapbox/streets-v11',
center: arrLocations[regionIndex][0][0], // starting position
zoom: 11 // starting zoom
});

//Display marker for starting point and that region
var popup1 = new mapboxgl.Popup({offset:25}).setText
    (
        "Starting Point"
    )

 let marker1 = new mapboxgl.Marker({color:"#FF0000"})
    .setLngLat(Object.values(arrLocations[regionIndex][0][0])) //Object values to overlook LAT and LNG 
    .setPopup(popup1)
    .addTo(map)

 //Adding a path line to the map
if (arrLocations[regionIndex][0].length ===3){
map.on('load', function () 
{
    map.addSource('region', 
    {
        'type': 'geojson',
        'data': 
        {
            'type': 'Feature',
            'properties': {},
            'geometry': 
            {
                'type': 'LineString',
                'coordinates': 
                    [   
                        Object.values(arrLocations[regionIndex][0][0]),
                        Object.values(arrLocations[regionIndex][0][1]),                        
                        Object.values(arrLocations[regionIndex][0][2])     
                    ]
            }   
        }
    });
map.addLayer({
'id': 'region',
'type': 'fill',
'source': 'region',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
});
}



//Adding a path line to the map
if (arrLocations[regionIndex][0].length ===4){
map.on('load', function () 
{
    map.addSource('region', 
    {
        'type': 'geojson',
        'data': 
        {
            'type': 'Feature',
            'properties': {},
            'geometry': 
            {
                'type': 'LineString',
                'coordinates': 
                    [   
                        Object.values(arrLocations[regionIndex][0][0]),   
                        Object.values(arrLocations[regionIndex][0][1]),                        
                        Object.values(arrLocations[regionIndex][0][2]), 
                        Object.values(arrLocations[regionIndex][0][3])
                    ]
            }   
        }
    });
map.addLayer({
'id': 'region',
'type': 'fill',
'source': 'region',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
});
}

if (arrLocations[regionIndex][0].length ===5){
map.on('load', function () 
{
    map.addSource('region', 
    {
        'type': 'geojson',
        'data': 
        {
            'type': 'Feature',
            'properties': {},
            'geometry': 
            {
                'type': 'LineString',
                'coordinates': 
                    [   
                        Object.values(arrLocations[regionIndex][0][0]),   
                        Object.values(arrLocations[regionIndex][0][1]),                      
                        Object.values(arrLocations[regionIndex][0][2]),   
                        Object.values(arrLocations[regionIndex][0][3]),
                        Object.values(arrLocations[regionIndex][0][4])
                    ]
            }   
        }
    });
map.addLayer({
'id': 'region',
'type': 'fill',
'source': 'region',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
});
}

if (arrLocations[regionIndex][0].length ===6){
map.on('load', function () 
{
    map.addSource('region', 
    {
        'type': 'geojson',
        'data': 
        {
            'type': 'Feature',
            'properties': {},
            'geometry': 
            {
                'type': 'LineString',
                'coordinates': 
                    [   
                        Object.values(arrLocations[regionIndex][0][0]), 
                        Object.values(arrLocations[regionIndex][0][1]),                         
                        Object.values(arrLocations[regionIndex][0][2]),     
                        Object.values(arrLocations[regionIndex][0][3]),
                        Object.values(arrLocations[regionIndex][0][4]),
                        Object.values(arrLocations[regionIndex][0][5])
                    ]
            }   
        }
    });
map.addLayer({
'id': 'region',
'type': 'fill',
'source': 'region',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
});
}

if (arrLocations[regionIndex][0].length ===7){
map.on('load', function () 
{
    map.addSource('region', 
    {
        'type': 'geojson',
        'data': 
        {
            'type': 'Feature',
            'properties': {},
            'geometry': 
            {
                'type': 'LineString',
                'coordinates': 
                    [   
                        Object.values(arrLocations[regionIndex][0][0]), 
                        Object.values(arrLocations[regionIndex][0][1]),                         
                        Object.values(arrLocations[regionIndex][0][2]),     
                        Object.values(arrLocations[regionIndex][0][3]),
                        Object.values(arrLocations[regionIndex][0][4]),
                        Object.values(arrLocations[regionIndex][0][5]),
                        Object.values(arrLocations[regionIndex][0][6])
                    ]
            }   
        }
    });
map.addLayer({
'id': 'region',
'type': 'fill',
'source': 'region',
'layout': {},
'paint': {
'fill-color': '#088',
'fill-opacity': 0.8
}
});
});
}

//Displays the Area and Perimeter of the Region
document.getElementById('Area').innerHTML = 'Area: '.bold() + regionInstance.area() + ' m' + '2'.sup();
document.getElementById('Perimeter').innerHTML = 'Perimeter: '.bold() + regionInstance.perimeter() + ' m';

//Button to delete the region
function removeRegion()
{
    //Asks
    if (confirm("Do you want to delete this region?"))//In case of misclick 
    {    
        //Remove this region from local storage
        retrieveStorage.splice(regionIndex, 1);
        retrieveStorage1.splice(regionIndex, 1);
        //Put the updated array into local storage(after removing a region) 
        localStorage.setItem("REGIONS", JSON.stringify(retrieveStorage));
        localStorage.setItem("REGIONSLIST", JSON.stringify(retrieveStorage1));
        alert("Region has been deleted. Returning to Homepage")
        location.href = "index.html"; // Goes back to index page 
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }  
}

function displayPosts() {
    let fencePosts = regionInstance.getfencePost(); //From Region class
    
    for (let i = 0; i < fencePosts.length; i++){
        let fencePosts = new mapboxgl.Marker() //Displays markers as fenceposts
        .setLngLat(fencePosts[i])
        .addTo(map);
        arrfencePosts.push(fencePosts); //Push number of markers to array
        }
        alert('Fencing required is: ' + fencePosts.length); //Display the number of markers in the array
}